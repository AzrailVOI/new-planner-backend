# Back-end application for [Azraїl PLANNER](https://planner.azrail.xyz)

## Requirements

- [Node.js](https://nodejs.org/en/)
- [PNPM](https://pnpm.io/)

## Technologies

- [TypeScript](https://www.typescriptlang.org/)
- [NestJS](https://nestjs.com/)
- [MySQL](https://www.mysql.com/)
## Installation

```bash
pnpm install
```

## Running the app

```bash
# development
pnpm run start

# watch mode
pnpm run start:dev

# production mode
pnpm run start:prod
```

## ENV examples
```
JWT_SECRET=?W?Bz9eaU7j?<rUb[Aq*[h)ga0abX*5M8ykbENV?0GgKTeS*DN@9TW
HOST=localhost
###
DATABASE_URL="mysql://1234:1234@localhost:3306/planner_db?schema=public"
DB_HOST=localhost
DB_USER=1234
DB_PASSWORD=1234
DB_DATABASE=planner_db
DB_PORT=3306
```

## License

[MIT licensed](LICENSE).
