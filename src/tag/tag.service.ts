import { Injectable } from '@nestjs/common'
import { TagDto } from './dto/tag.dto'
import { createId } from '@paralleldrive/cuid2'
import { MysqlService } from '../mysql/mysql.service'

@Injectable()
export class TagService {
	constructor(private mysql: MysqlService) {}
	async create(dto: TagDto, userId: string) {
		const id = createId()
		const createSql = `INSERT INTO tag (id, tag, user_id) VALUES (?, ?, ?);`
		await this.mysql.query(createSql, [id, dto.tag.toLowerCase(), userId])

		return this.findAll(userId)
	}

	findAll(userId: string) {
		const sql = 'SELECT id, tag FROM tag WHERE user_id = ?;'
		return this.mysql.query(sql, [userId])
	}

	findByName(name: string, userId: string) {
		const sql = 'SELECT id, tag FROM tag WHERE tag = ? AND user_id = ?;'
		return this.mysql.queryUnique(sql, [name, userId])
	}

	update(id: string, dto: TagDto, userId: string) {
		const updateSql = `UPDATE tag SET tag = ? WHERE id = ? AND user_id = ?;`
		return this.mysql.query(updateSql, [dto.tag, id, userId])
	}

	remove(id: string, userId: string) {
		return this.mysql.query('DELETE FROM tag WHERE id = ? AND user_id = ?', [
			id,
			userId
		])
	}
}
