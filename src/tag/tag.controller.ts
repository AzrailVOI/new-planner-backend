import {
	Controller,
	Get,
	Post,
	Body,
	Param,
	Delete,
	UsePipes,
	ValidationPipe,
	HttpCode,
	Put
} from '@nestjs/common'
import { TagService } from './tag.service'
import { TagDto } from './dto/tag.dto'
import { CurrentUser } from '../decorators/user.decorator'
import { Auth } from '../decorators/auth.decorator'

@Controller('user/tag')
export class TagController {
	constructor(private readonly tagService: TagService) {}

	@UsePipes(new ValidationPipe())
	@HttpCode(200)
	@Post()
	@Auth()
	create(@Body() createTagDto: TagDto, @CurrentUser('id') userId: string) {
		return this.tagService.create(createTagDto, userId)
	}

	@UsePipes(new ValidationPipe())
	@HttpCode(200)
	@Auth()
	@Get()
	findAll(@CurrentUser('id') userId: string) {
		return this.tagService.findAll(userId)
	}

	@UsePipes(new ValidationPipe())
	@HttpCode(200)
	@Auth()
	@Get(':name')
	findByName(@Param('name') name: string, @CurrentUser('id') userId: string) {
		return this.tagService.findByName(name, userId)
	}

	@UsePipes(new ValidationPipe())
	@HttpCode(204)
	@Auth()
	@Put(':id')
	update(
		@Param('id') id: string,
		@Body() updateTagDto: TagDto,
		@CurrentUser('id') userId: string
	) {
		return this.tagService.update(id, updateTagDto, userId)
	}

	@UsePipes(new ValidationPipe())
	@HttpCode(204)
	@Auth()
	@Delete(':id')
	remove(@Param('id') id: string, @CurrentUser('id') userId: string) {
		return this.tagService.remove(id, userId)
	}
}
