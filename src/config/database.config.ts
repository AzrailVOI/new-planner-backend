import { ConfigService } from '@nestjs/config'

export const getDatabaseConfig = (configService: ConfigService) => ({
	host: configService.get('DB_HOST'),
	port: configService.get('DB_PORT'),
	user: configService.get('DB_USER'),
	password: configService.get('DB_PASSWORD'),
	database: configService.get('DB_DATABASE')
})
