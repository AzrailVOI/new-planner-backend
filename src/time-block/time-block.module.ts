import { Module } from '@nestjs/common'
import { TimeBlockService } from './time-block.service'
import { TimeBlockController } from './time-block.controller'
import {MysqlModule} from "../mysql/mysql.module";

@Module({
	imports: [MysqlModule],
	controllers: [TimeBlockController],
	providers: [TimeBlockService],
	exports: [TimeBlockService]
})
export class TimeBlockModule {}
