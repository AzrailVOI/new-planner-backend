import { Injectable } from '@nestjs/common'
import { TimeBlockDto } from './dto/time-block.dto'
import { MysqlService } from '../mysql/mysql.service'
import { createId } from '@paralleldrive/cuid2'

@Injectable()
export class TimeBlockService {
	constructor(private mysql: MysqlService) {}

	async create(dto: TimeBlockDto, userId: string) {
		const insertParams = []
		insertParams.push(createId())

		if (dto.name) {
			insertParams.push(dto.name)
		}
		if (dto.color) {
			insertParams.push(dto.color)
		}
		if (dto.duration) {
			insertParams.push(dto.duration)
		}
		if (dto.order) {
			insertParams.push(dto.order)
		}
		insertParams.push(userId)

		const insertSql = `INSERT INTO time_block (id, ${dto.name ? 'name,' : ''} ${dto.color ? 'color,' : ''} ${dto.duration ? 'duration,' : ''} ${dto.order ? '`order`,' : ''} user_id) 
VALUES (${insertParams.map(() => '?').join(',')});`

		// console.log('insertParams', insertParams)
		// console.log('insertSql', insertSql)

		await this.mysql.query(insertSql, insertParams)
		// console.log('inserted', insertParams)

		const selectSql = `SELECT * FROM time_block WHERE id = ?;`

		// const result = await this.mysql.queryUnique(selectSql)
		// console.log('result', result)

		return this.mysql.queryUnique(selectSql, [insertParams[0]])
	}

	async update(
		dto: Partial<TimeBlockDto>,
		timeBlockId: string,
		userId: string
	) {
		const updateSql = `UPDATE time_block
SET
  ${dto.name ? 'name = ?' : ''}
  ${dto.color ? ',color = ?' : ''}
  ${dto.duration ? ',duration = ?' : ''}
  ${dto.order ? ',`order` = ?' : ''}
WHERE
  user_id = ?
  AND id = ?;
`

		const updateParams = []
		if (dto.name) {
			updateParams.push(dto.name)
		}
		if (dto.color) {
			updateParams.push(dto.color)
		}
		if (dto.duration) {
			updateParams.push(dto.duration)
		}
		if (dto.order) {
			updateParams.push(dto.order)
		}
		updateParams.push(userId)
		updateParams.push(timeBlockId)

		// console.log('updateSql', updateSql)
		// console.log('updateParams', updateParams)
		await this.mysql.query(updateSql, updateParams)
		// console.log('updated', updateParams)

		const selectSql = `SELECT * FROM time_block WHERE id = ?;`

		return this.mysql.queryUnique(selectSql, [timeBlockId])
	}

	async delete(timeBlockId: string) {
		const deleteSql = `DELETE FROM time_block WHERE id = ?;`

		await this.mysql.query(deleteSql, [timeBlockId])

		return
	}

	async getAll(userId: string) {
		const selectSql =
			'SELECT * FROM time_block WHERE user_id = ? ORDER BY `order` ASC;'

		return this.mysql.query(selectSql, [userId])
	}

	async updateOrder(ids: string[], userId: string) {
		const transactionSql = `
UPDATE time_block
SET \`order\` = CASE
${ids.map((id, i) => `WHEN id = '${id}' THEN ${i}`).join('\n')}
END
WHERE id IN (${ids.map(() => '?').join(',')}) AND user_id = ?;`

		await this.mysql.transaction([transactionSql], [[...ids, userId]])

		const selectUpdatedOrderSql = `SELECT * FROM time_block WHERE id IN (${ids.map(() => '?').join(',')});`

		return this.mysql.query(selectUpdatedOrderSql, ids)
	}
}
