import { Priority } from './database.types'

export interface ITaskResponse {
	id: string
	name: string
	isCompleted: boolean
	description: string
	columnId: string
	priority: Priority | null
	tags: ITags[]
}
// id: string
// name: string
// priority?: Priority | null
// isCompleted?: boolean | null
// description?: string | null
// userId?: string
// columnId?: string | null
// tags?: string[] | null
// tagIds?: string[] | null
export interface ITags {
	id: string
	tag: string
}
