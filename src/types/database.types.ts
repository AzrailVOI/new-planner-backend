export interface UserSettings {
	id: string
	workInterval?: number | null
	breakInterval?: number | null
	intervalsCount?: number | null
	userId: string
}

export interface User {
	id: string
	email: string
	name?: string | null
	password: string
	tasks: Task[]
	timeblocks?: TimeBlock[]
	pomodorosessions?: PomodoroSession[]
	UserSettings?: UserSettings[]
}

export interface Task {
	id: string
	name: string
	priority?: Priority | null
	isCompleted?: boolean | null
	description?: string | null
	userId?: string
	columnId?: string | null
	tags?: string[] | null
	tagIds?: string[] | null
}

export interface TaskColumn {
	id: string
	name?: string | null
	order?: number | null
	userId: string
}

export enum Priority {
	low = 'low',
	medium = 'medium',
	high = 'high'
}

export interface TimeBlock {
	id: string
	name: string
	color?: string | null
	duration: number
	order: number
	userId: string
}

export interface PomodoroSession {
	id: string
	isCompleted?: boolean | null
	userId: string
	rounds: PomodoroRound[]
}

export interface PomodoroRound {
	id: string
	total_seconds: number
	isCompleted?: boolean | null
	pomodoro_sessionId: string
}
