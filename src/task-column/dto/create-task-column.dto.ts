import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator'

export class CreateTaskColumnDto {
	@IsNotEmpty()
	@IsString({ message: 'Name must be a string' })
	name: string

	@IsOptional()
	@IsNumber()
	order: number
}
