import { PartialType } from '@nestjs/mapped-types'
import { CreateTaskColumnDto } from './create-task-column.dto'
import { IsOptional, IsString } from 'class-validator'

export class UpdateTaskColumnDto extends PartialType(CreateTaskColumnDto) {
	@IsString({ message: 'Name must be a string' })
	@IsOptional()
	name: string
}
