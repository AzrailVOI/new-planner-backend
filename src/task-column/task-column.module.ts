import { Module } from '@nestjs/common'
import { TaskColumnService } from './task-column.service'
import { TaskColumnController } from './task-column.controller'
import {MysqlModule} from "../mysql/mysql.module";

@Module({
	imports: [MysqlModule],
	controllers: [TaskColumnController],
	providers: [TaskColumnService]
})
export class TaskColumnModule {}
