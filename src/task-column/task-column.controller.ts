import {
	Controller,
	Get,
	Post,
	Body,
	Param,
	Delete,
	UsePipes,
	ValidationPipe,
	HttpCode,
	Put
} from '@nestjs/common'
import { TaskColumnService } from './task-column.service'
import { CreateTaskColumnDto } from './dto/create-task-column.dto'
import { UpdateTaskColumnDto } from './dto/update-task-column.dto'
import { Auth } from '../decorators/auth.decorator'
import { CurrentUser } from '../decorators/user.decorator'
import { UpdateOrderDto } from '../time-block/dto/update-order.dto'

@Controller('user/task-column')
export class TaskColumnController {
	constructor(private readonly taskColumnService: TaskColumnService) {}

	@UsePipes(new ValidationPipe())
	@HttpCode(201)
	@Post()
	@Auth()
	create(
		@Body() createTaskColumnDto: CreateTaskColumnDto,
		@CurrentUser('id') userId: string
	) {
		return this.taskColumnService.create(createTaskColumnDto, userId)
	}

	@HttpCode(200)
	@Get()
	@Auth()
	findAll(@CurrentUser('id') userId: string) {
		return this.taskColumnService.findAll(userId)
	}

	@UsePipes(new ValidationPipe())
	@HttpCode(204)
	@Put('update-order')
	@Auth()
	async updateOrder(
		@Body() updateOrderDto: UpdateOrderDto,
		@CurrentUser('id') userId: string
	) {
		return this.taskColumnService.updateOrder(updateOrderDto.ids, userId)
	}
	@UsePipes(new ValidationPipe())
	@HttpCode(204)
	@Put(':id')
	@Auth()
	update(
		@Param('id') id: string,
		@Body() updateTaskColumnDto: UpdateTaskColumnDto
	) {
		return this.taskColumnService.update(id, updateTaskColumnDto)
	}

	@HttpCode(204)
	@Delete(':id')
	@Auth()
	remove(@Param('id') id: string) {
		return this.taskColumnService.remove(id)
	}
}
