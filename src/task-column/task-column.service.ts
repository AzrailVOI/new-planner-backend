import { Injectable } from '@nestjs/common'
import { CreateTaskColumnDto } from './dto/create-task-column.dto'
import { UpdateTaskColumnDto } from './dto/update-task-column.dto'
import { MysqlService } from '../mysql/mysql.service'
import { createId } from '@paralleldrive/cuid2'

@Injectable()
export class TaskColumnService {
	constructor(private mysql: MysqlService) {}
	async create(dto: CreateTaskColumnDto, userId: string) {
		const columns = await this.findAll(userId)
		const columnId = createId()
		const createParams = []
		createParams.push(columnId)
		if (dto.name) createParams.push(dto.name)
		if (dto.order) createParams.push(dto.order)
		else createParams.push(columns.length > 0 ? columns.length : 99)
		createParams.push(userId)
		const createSql = `
    INSERT INTO task_column (id, ${dto.name ? 'name,' : ''} \`order\`, user_id) VALUES (${createParams.map(() => '?').join(',')})
    `
		// console.log('createSql', createSql)
		// console.log('createParams', createParams)
		return this.mysql.query(createSql, createParams)
	}

	findAll(userId: string) {
		const sql = `SELECT id, name, \`order\` FROM task_column WHERE user_id = ? ORDER BY \`order\` ASC;`
		return this.mysql.query(sql, [userId])
	}

	update(id: string, dto: UpdateTaskColumnDto) {
		const updateParams = []
		let updateSql = 'UPDATE task_column SET'

		if (dto.name) {
			updateSql += ' name = ?,'
			updateParams.push(dto.name)
		}

		if (dto.order !== undefined) {
			updateSql += ' `order` = ?,'
			updateParams.push(dto.order)
		}

		// Убираем лишнюю запятую в конце, если она есть
		updateSql = updateSql.replace(/,$/, '')

		updateParams.push(id) // Добавляем id в конец списка параметров

		// В конце добавляем условие WHERE
		updateSql += ' WHERE id = ?'
		return this.mysql.query(updateSql, updateParams)
	}

	remove(id: string) {
		const deleteSql = 'DELETE FROM task_column WHERE id = ?'
		return this.mysql.query(deleteSql, [id])
	}

	async updateOrder(ids: string[], userId: string) {
		// console.log('ids', ids)
		// console.log('userId', userId)
		const updateSql = `
UPDATE task_column
SET \`order\` = CASE
${ids.map((id, i) => `WHEN id = '${id}' THEN ${i}`).join('\n')}
END
WHERE id IN (${ids.map(() => '?').join(',')}) AND user_id = ?;`

		return this.mysql.transaction([updateSql], [[...ids, userId]])
	}
}
