import { Injectable } from '@nestjs/common'
import { TagToTaskDto } from './dto/create-tag-to-task.dto'
import { MysqlService } from '../mysql/mysql.service'
@Injectable()
export class TagToTaskService {
	constructor(private readonly mysql: MysqlService) {}
	create(dto: TagToTaskDto) {
		const createSql = `INSERT INTO tag_to_task (tag_id, task_id) VALUES (?, ?);`
		return this.mysql.query(createSql, [dto.tagId, dto.taskId])
	}

	tagToTask(userId: string) {
		const sql = `SELECT tag_id AS tagId, task_id AS taskId FROM planner_db.tag_to_task
join task on task_id = task.id
where user_id = ?`
		return this.mysql.query(sql, [userId])
	}

	findByTag(tagId: string) {
		const sql = `SELECT tag_id AS tagId, task_id AS taskId FROM tag_to_task WHERE tag_id = ?`
		return this.mysql.query(sql, [tagId])
	}

	remove(tagId: string, taskId: string) {
		const sql = `DELETE FROM tag_to_task WHERE tag_id = ? AND task_id = ?`
		return this.mysql.query(sql, [tagId, taskId])
	}
}
