import { Module } from '@nestjs/common'
import { TagToTaskService } from './tag-to-task.service'
import { TagToTaskController } from './tag-to-task.controller'
import {MysqlModule} from "../mysql/mysql.module";

@Module({
	imports: [
		MysqlModule
	],
	controllers: [TagToTaskController],
	providers: [TagToTaskService]
})
export class TagToTaskModule {}
