import {Controller, Get, Post, Body, Delete, Query, Param} from '@nestjs/common'
import { TagToTaskService } from './tag-to-task.service'
import { TagToTaskDto } from './dto/create-tag-to-task.dto'
import {CurrentUser} from "../decorators/user.decorator";
import {Auth} from "../decorators/auth.decorator";

@Controller('user/tag-to-task')
export class TagToTaskController {
	constructor(private readonly tagToTaskService: TagToTaskService) {}
	@Auth()
	@Post()
	create(@Body() dto: TagToTaskDto) {
		return this.tagToTaskService.create(dto)
	}

	@Auth()
	@Get()
	tagToTaskCount(@CurrentUser('id') userId: string) {
		return this.tagToTaskService.tagToTask(userId)
	}
	@Auth()
	@Get(':id')
	findByTag(@Param('id') id: string) {
		return this.tagToTaskService.findByTag(id)
	}
	@Auth()
	@Delete('?')
	remove(@Query('tagId') tagId: string, @Query('taskId') taskId: string) {
		return this.tagToTaskService.remove(tagId, taskId)
	}
}
