import { IsNotEmpty, IsString } from 'class-validator'

export class TagToTaskDto {
	@IsNotEmpty()
	@IsString()
	tagId: string

	@IsNotEmpty()
	@IsString()
	taskId: string
}
