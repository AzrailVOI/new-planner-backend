import { Injectable } from '@nestjs/common'
import { Task, User } from '../types/database.types'

@Injectable()
export class TransformService {
	transformGetUserById(rows: any[]): User | null {
		if (!rows || rows.length === 0) {
			return null
		}

		const user: User = {
			id: rows[0].user_id,
			email: rows[0].email,
			name: rows[0].name,
			password: rows[0].password,
			tasks: []
		}

		rows.forEach(row => {
			const task: Task = {
				id: row.task_id,
				name: row.task_name,
				priority: row.priority,
				isCompleted: Boolean(row.isCompleted),
				columnId: row.task_column_id,
				description: row.task_description
			}
			user.tasks.push(task)
		})

		return user
	}
}
