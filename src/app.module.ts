import { Module } from '@nestjs/common'
import { AuthModule } from './auth/auth.module'
import { UserModule } from './user/user.module'
import { ConfigModule } from '@nestjs/config'
import { TaskModule } from './task/task.module'
import { TimeBlockModule } from './time-block/time-block.module'
import { GetOriginController } from './get-origin/get-origin.controller'
import { TransformService } from './transform/transform.service'
import { TaskColumnModule } from './task-column/task-column.module'
import { TagModule } from './tag/tag.module'
import { TagToTaskModule } from './tag-to-task/tag-to-task.module'
import { MysqlModule } from './mysql/mysql.module'

@Module({
	imports: [
		ConfigModule.forRoot(),
		AuthModule,
		UserModule,
		TaskModule,
		TimeBlockModule,
		TaskColumnModule,
		TagModule,
		TagToTaskModule,
		MysqlModule
	],
	controllers: [GetOriginController],
	providers: [TransformService]
})
export class AppModule {}
