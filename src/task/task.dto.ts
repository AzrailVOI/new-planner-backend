import { IsBoolean, IsEnum, IsOptional, IsString } from 'class-validator'
import { Priority } from '../types/database.types'
import { Transform } from 'class-transformer'

export class TaskDto {
	@IsOptional()
	@IsString()
	name: string

	@IsOptional()
	@IsBoolean()
	isCompleted?: boolean

	@IsString()
	columnId?: string

	@IsEnum(Priority)
	@IsOptional()
	@Transform(({ value }) => ('' + value).toLowerCase())
	priority?: Priority

	@IsOptional()
	@IsString()
	description?: string
}
