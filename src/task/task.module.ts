import { Module } from '@nestjs/common'
import { TaskService } from './task.service'
import { TaskController } from './task.controller'
import {MysqlModule} from "../mysql/mysql.module";

@Module({
	imports: [MysqlModule],
	controllers: [TaskController],
	providers: [TaskService],
	exports: [TaskService]
})
export class TaskModule {}
