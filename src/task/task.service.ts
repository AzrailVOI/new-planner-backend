import { Injectable } from '@nestjs/common'
import { TaskDto } from './task.dto'
import { MysqlService } from '../mysql/mysql.service'
import { createId } from '@paralleldrive/cuid2'
import { ITags, ITaskResponse } from '../types/task-response.types'
import { Task } from '../types/database.types'
@Injectable()
export class TaskService {
	constructor(private mysql: MysqlService) {}

	async create(dto: TaskDto, userId: string) {
		const newTaskId = createId()
		const createParams = []
		const columns = ['id']
		createParams.push(newTaskId)
		if (dto.name) {
			columns.push('name')
			createParams.push(dto.name)
		}
		if (dto.isCompleted !== undefined) {
			columns.push('is_completed')
			createParams.push(dto.isCompleted)
		}
		if (dto.columnId) {
			columns.push('column_id')
			createParams.push(dto.columnId)
		}
		if (dto.priority) {
			columns.push('priority')
			createParams.push(dto.priority)
		}
		if (dto.description) {
			columns.push('description')
			createParams.push(dto.description)
		}
		columns.push('user_id')
		createParams.push(userId)

		const createSql = `INSERT INTO task (${columns.join(', ')}) VALUES (${createParams.map(() => '?').join(', ')});`
		// console.log('createTaskSql', createSql)
		// console.log('createTaskParams', createParams)

		return this.mysql.query(createSql, createParams)
	}

	async update(dto: Partial<TaskDto>, taskId: string, userId: string) {
		let updateSql = `UPDATE task SET`

		const updateParams = []

		if (dto.name) {
			updateSql += ` name = ?,`
			updateParams.push(dto.name)
		}
		if (dto.isCompleted !== undefined) {
			updateSql += ` is_completed = ?,`
			updateParams.push(dto.isCompleted)
		}
		if (dto.priority) {
			updateSql += ` priority = ?,`
			updateParams.push(dto.priority)
		}
		if (dto.columnId) {
			updateSql += ` column_id = ?,`
			updateParams.push(dto.columnId)
		}
		if (dto.description !== undefined) {
			updateSql += ` description = ?,`
			updateParams.push(dto.description)
		}

		// Уберите последнюю запятую, если она есть
		updateSql = updateSql.replace(/,$/, '')

		// Добавляем условия WHERE
		updateSql += ` WHERE user_id = ? AND id = ?;`
		updateParams.push(userId, taskId)

		return this.mysql.query(updateSql, updateParams)
	}

	async delete(taskId: string) {
		const deleteSql = `DELETE FROM task WHERE id = ?;`
		await this.mysql.query(deleteSql, [taskId])
	}

	async getAll(userId: string) {
		const sql = `SELECT task.id, task.name, task.is_completed AS isCompleted, task.description, task.column_id AS columnId, task.priority, GROUP_CONCAT(tag.id SEPARATOR ';') AS tagIds, GROUP_CONCAT(tag.tag SEPARATOR ';') AS tags
FROM task
LEFT JOIN tag_to_task ON task.id = tag_to_task.task_id
LEFT JOIN tag ON tag_to_task.tag_id = tag.id
WHERE task.user_id = ?
GROUP BY task.id;
`
		const tasks = await this.mysql.query(sql, [userId])
		const rightTasks = tasks.map(task => {
			return {
				...task,
				tags: task.tags !== null ? task.tags.split(';') : null,
				tagIds: task.tagIds !== null ? task.tagIds.split(';') : null
			}
		}) as Array<Task>

		let tasksResponse: Array<ITaskResponse> = []
		tasksResponse = rightTasks.map(task => {
			const tagsResponse: Array<ITags> = []
			if (task.tags !== null) {
				for (let i = 0; i < task.tags.length; i++) {
					tagsResponse.push({
						id: task.tagIds[i],
						tag: task.tags[i]
					})
				}
			}
			return {
				id: task.id,
				name: task.name,
				priority: task.priority,
				isCompleted: !!task.isCompleted,
				description: task.description,
				columnId: task.columnId,
				tags: tagsResponse
			}
		})
		const filteredTasksResponse = tasksResponse.filter(
			task => !task.isCompleted
		)
		filteredTasksResponse.push(
			...tasksResponse.filter(task => !!task.isCompleted)
		)
		return filteredTasksResponse
	}

	async getCompletedCount(userId: string) {
		const sql = `SELECT COUNT(*) AS count FROM task WHERE user_id = ? AND is_completed = 1;`
		return this.mysql.queryUnique(sql, [userId])
	}

	async getUncompletedCount(userId: string) {
		const sql = `SELECT COUNT(*) AS count FROM task WHERE user_id = ? AND is_completed = 0;`
		return this.mysql.queryUnique(sql, [userId])
	}
}
