import { Module } from '@nestjs/common';
import {MysqlService} from "./mysql.service";
import {ConfigService} from "@nestjs/config";

@Module({
    providers: [MysqlService, ConfigService],
    exports: [MysqlService],
})
export class MysqlModule {}
