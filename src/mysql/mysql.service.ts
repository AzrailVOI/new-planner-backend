import { Injectable } from '@nestjs/common'
import * as mysql from 'mysql2/promise'
import { getDatabaseConfig } from '../config/database.config'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class MysqlService {
	private pool

	constructor(private configService: ConfigService) {
		this.pool = mysql.createPool(getDatabaseConfig(configService))
	}

	async query(sql: string, values?: any[]) {
		let connection
		try {
			connection = await this.pool.getConnection()
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			const [rows, fields] = await connection.query(sql, values)
			return rows
		} catch (error) {
			throw error
		} finally {
			if (connection) {
				connection.release()
			}
		}
	}

	async queryUnique(sql: string, values?: any[]) {
		const rows = await this.query(sql, values)
		return rows[0]
	}

	async startTransaction() {
		const sql = `START TRANSACTION;`
		await this.query(sql)
	}

	async commitTransaction() {
		const sql = `COMMIT;`
		await this.query(sql)
	}

	async transaction(sqls: string[], values?: Array<Array<any>>) {
		await this.startTransaction()
		for (let i = 0; i < sqls.length; i++) {
			const sql = sqls[i]
			const value = values ? values[i] : []
			await this.query(sql, value)
		}
		await this.commitTransaction()
	}
}
