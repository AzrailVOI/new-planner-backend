import { Injectable } from '@nestjs/common'
import { AuthDto } from '../auth/dto/auth.dto'
import { hash } from 'argon2'
import { UserDto } from './dto/user.dto'
import { TaskService } from '../task/task.service'
import { MysqlService } from '../mysql/mysql.service'
import { TransformService } from '../transform/transform.service'
import { createId } from '@paralleldrive/cuid2'

@Injectable()
export class UserService {
	constructor(
		private task: TaskService,
		private mysql: MysqlService,
		private transform: TransformService
	) {}

	async getById(id: string) {
		const sql = `CALL get_user_with_tasks(?);`
		const user = await this.mysql.query(sql, [id])
		return this.transform.transformGetUserById(user[0])
	}

	async getByEmail(email: string) {
		const sql = `SELECT *
FROM user
WHERE email = ?;
`
		return this.mysql.queryUnique(sql, [email])
	}

	async getIntervalCountsById(id: string) {
		const sql = `SELECT intervals_count
FROM user
WHERE id = ?;
`
		return this.mysql.queryUnique(sql, [id])
	}

	async getProfile(id: string) {
		const profile = await this.getById(id)

		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		const { password, ...rest } = profile

		const completedTasks = (await this.task.getCompletedCount(id)).count
		const uncompletedTasks = (await this.task.getUncompletedCount(id)).count
		// console.log(totalTasks, totalCompletedTasks, todayTasks, weekTasks)

		return {
			user: rest,
			statistics: [
				{
					label: 'Completed tasks',
					value: completedTasks
				},
				{
					label: 'Uncompleted tasks',
					value: uncompletedTasks
				}
			]
		}
	}

	async create(dto: AuthDto) {
		const user = {
			email: dto.email,
			name: 'Anonymous',
			password: await hash(dto.password)
		}
		const id = createId()
		// Выполнение запроса на вставку
		const insertSql = `INSERT INTO user (id, email, name, password) VALUES (?, ?, ?, ?)`
		await this.mysql.query(insertSql, [
			id,
			user.email,
			user.name,
			user.password
		])
		const selectUser = `SELECT * FROM user WHERE id = ?`
		return this.mysql.queryUnique(selectUser, [id])
	}

	async update(id: string, dto: UserDto) {
		let data = dto

		if (dto.password) {
			data = { ...dto, password: await hash(dto.password) }
		}

		let updateSql = 'UPDATE user SET '
		const updateFields = []

		if (data.name) {
			updateFields.push('name = ?')
		}

		if (data.email) {
			updateFields.push('email = ?')
		}

		if (data.password) {
			updateFields.push('password = ?')
		}

		updateSql += updateFields.join(', ')
		updateSql += ' WHERE id = ?'

		const updateValues = []
		if (data.name) {
			updateValues.push(data.name)
		}

		if (data.email) {
			updateValues.push(data.email)
		}

		if (data.password) {
			updateValues.push(data.password)
		}

		updateValues.push(id)

		return this.mysql.query(updateSql, updateValues)
	}
}
