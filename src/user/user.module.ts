import { Module } from '@nestjs/common'
import { UserService } from './user.service'
import { UserController } from './user.controller'
import { TaskModule } from '../task/task.module'
import { TransformService } from '../transform/transform.service'
import { MysqlModule } from '../mysql/mysql.module'

@Module({
	imports: [TaskModule, MysqlModule],
	controllers: [UserController],
	providers: [UserService, TransformService],
	exports: [UserService]
})
export class UserModule {}
