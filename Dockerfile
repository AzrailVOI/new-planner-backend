FROM node

EXPOSE 4200
COPY . /backend
WORKDIR /backend

RUN npm install
CMD ["npm", "start"]
